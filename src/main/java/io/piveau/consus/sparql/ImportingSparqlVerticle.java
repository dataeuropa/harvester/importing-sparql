package io.piveau.consus.sparql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.consus.utils.SparqlConnector;
import io.piveau.pipe.PipeContext;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class ImportingSparqlVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.sparql.queue";

    private static final String ENV_PIVEAU_IMPORTING_SEND_LIST_DELAY = "PIVEAU_IMPORTING_SEND_LIST_DELAY";

    private int defaultDelay = 8000;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);

        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray().add(ENV_PIVEAU_IMPORTING_SEND_LIST_DELAY)));
        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions));
        retriever.getConfig()
                .onSuccess(config -> {
                    defaultDelay = config.getInteger(ENV_PIVEAU_IMPORTING_SEND_LIST_DELAY, defaultDelay);
                    startPromise.complete();
                })
                .onFailure(startPromise::fail);

        retriever.listen(change -> defaultDelay = change.getNewConfiguration().getInteger(ENV_PIVEAU_IMPORTING_SEND_LIST_DELAY, 8000));
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        JsonObject config = pipeContext.getConfig();
        pipeContext.log().info("Import started.");

        SparqlConnector connector = SparqlConnector.create(vertx, pipeContext);

        connector.getTotalCount()
                .compose(totalCount -> {
                    connector.init(totalCount);
                    return Future.future(connector::fetchPage);
                })
                .onSuccess(v -> {
                    pipeContext.log().info("Import metadata finished");
                    int delay = pipeContext.getConfig().getInteger("sendListDelay", defaultDelay);
                    vertx.setTimer(delay, t -> {
                        ObjectNode info = new ObjectMapper().createObjectNode()
                                .put("content", "identifierList")
                                .put("catalogue", config.getString("catalogue"));
                        pipeContext.setResult(new JsonArray(connector.getIdentifiers()).encodePrettily(), "application/json", info).forward();
                    });

                })
                .onFailure(cause -> pipeContext.log().error("Import metadata finished", cause));
    }

}

package io.piveau.consus.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.dcatap.TripleStore;
import io.piveau.pipe.PipeContext;
import io.piveau.rdf.Piveau;
import io.piveau.rdf.RDFMimeTypes;
import io.piveau.utils.JenaUtils;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCAT;

import java.util.ArrayList;
import java.util.List;

public class SparqlConnector {

    private static final int LIMIT = 100;
    private long offset = 0;

    private final String catalogue;

    private long totalCount = 0;
    private final List<String> identifiers = new ArrayList<>();

    private final String outputFormat;

    private final PipeContext pipeContext;

    private final TripleStore tripleStore;

    private String countQuery = "SELECT (COUNT(?s) AS ?count) WHERE { ?s a <" + DCAT.Dataset + "> }";
    private String graphQuery = "SELECT ?graph WHERE { { SELECT DISTINCT ?graph WHERE { graph ?graph { ?s a <" + DCAT.Dataset + "> } } ORDER BY ?graph } }";

    public static SparqlConnector create(Vertx vertx, PipeContext pipeContext) {
        return new SparqlConnector(vertx, pipeContext);
    }

    private SparqlConnector(Vertx vertx, PipeContext pipeContext) {
        this.pipeContext = pipeContext;

        JsonObject config = pipeContext.getConfig();

        JsonObject sparqlEndpoint = config.getJsonObject("sparqlEndpoint");

        tripleStore = new TripleStore(vertx, sparqlEndpoint, null, pipeContext.getPipe().getHeader().getName());

        catalogue = config.getString("catalogue");

        outputFormat = config.getString("outputFormat", RDFMimeTypes.NTRIPLES);

        if (config.containsKey("queries")) {
            JsonObject queries = config.getJsonObject("queries", new JsonObject());
            if (queries.containsKey("countQuery")) {
                countQuery = queries.getString("countQuery", countQuery);
            }
            if (queries.containsKey("graphQuery")) {
                graphQuery = queries.getString("graphQuery", graphQuery);
            }
        }
    }

    public Future<Long> getTotalCount() {
        return tripleStore.select(countQuery)
                .map(result -> result.next().getLiteral("count").getLong());
    }

    public List<String> getIdentifiers() {
        return identifiers;
    }

    public void init(long totalCount) {
        offset = 0;
        identifiers.clear();
        this.totalCount = totalCount;
    }

    public void fetchPage(Promise<Void> promise) {
        getGraphList()
                .onSuccess(graphNames -> {
                    if (graphNames.isEmpty()) {
                        promise.complete();
                    } else {
                        graphNames.forEach(graphName -> tripleStore.getGraph(graphName)
                                .onSuccess(dataset -> {
                                    Resource metadata = dataset.getResource(graphName);
                                    String identifier = JenaUtils.findIdentifier(metadata);
                                    identifiers.add(identifier);
                                    Model extracted = Piveau.extractAsModel(metadata);
                                    ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                                            .put("total", totalCount)
                                            .put("counter", identifiers.size())
                                            .put("identifier", identifier)
                                            .put("catalogue", catalogue);

                                    pipeContext.setResult(JenaUtils.write(extracted, outputFormat), outputFormat, dataInfo).forward();
                                    pipeContext.log().info("Data imported: {}", dataInfo);

                                    dataset.close();
                                })
                                .onFailure(cause -> pipeContext.log().error("Fetch dataset graph", cause))
                        );
                        offset += LIMIT;
                        if (offset < totalCount) {
                            fetchPage(promise);
                        } else {
                            promise.complete();
                        }
                    }
                })
                .onFailure(promise::fail);
    }

    private Future<List<String>> getGraphList() {
        String query = graphQuery + " OFFSET " + offset + " LIMIT " + LIMIT;
        return tripleStore.select(query)
                .map(list -> {
                    ArrayList<String> solutions = new ArrayList<>();
                    list.forEachRemaining(solution -> solutions.add(solution.getResource("graph").getURI()));
                    return solutions;
                });
    }

}

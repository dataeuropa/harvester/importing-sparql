# Pipe Segment Configuration

## _mandatory_

* `sparqlEndpoint`

  Configuration of the sparql endpoint
  ```json
  {
    "sparqlEndpoint": {
      "address": "https://example.com",
      "graphEndpoint": "/sparql-graph-crud",
      "graphAuthEndpoint": "/sparql-graph-crud-auth",
      "queryEndpoint": "/sparql",
      "queryAuthEndpoint": "/sparql-auth",
      "username": "",
      "password": ""
    }
  }
  ```

* `catalogue`

  The id of the target catalogue

## _optional_

* `outputFormat`

  Mimetype to use for payload. Default is `application/n-triples`

  Possible output formats are:

    * `application/rdf+xml`
    * `application/n-triples`
    * `application/ld+json`
    * `application/trig`
    * `text/turtle`
    * `text/n3`

* `sendListDelay`

  The delay in milliseconds before the list of identifiers is sent. Take precedence over service configuration (see `PVEAU_IMPORTING_SEND_LIST_DELAY`)

* `queries`

  Override the default queries. A JSON object containing one query for the total count of datasets and one for fetching the list of dataset graphs.
  The final count variable needs to be named `count`, a graph uri needs tpo be named `graph`. Pagination is implicit.
  ```json
  {
    "queries": {
      "countQuery": "SELECT (COUNT(?dataset) AS ?count) WHERE { ?dataset a <http://www.w3.org/ns/dcat#Dataset> }",
      "graphQuery": "SELECT ?graph WHERE { { SELECT DISTINCT ?graph WHERE { graph ?graph { ?s a <http://www.w3.org/ns/dcat#Dataset> } } ORDER BY ?graph } }"
    }
  }
  ```

# Data Info Object

* `total`

  Total number of datasets

* `counter`

  The number of this dataset

* `identifier`

  The unique identifier in the source of this dataset

* `catalogue`

  The id of the target catalogue

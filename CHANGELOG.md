# ChangeLog

## 2.0.1 (2023-11-13)

**Changed:**
* Utils dependency update with increased json string size limit

## 2.0.0 (2023-10-05)

**Added:**
* Pipe configuration parameter `queries` for sparql queries 

**Changed:**
* Refactor SparqlConnector, using futures and promises instead of handlers 

**Removed:**
* `catalogueUriRef` pipe segment configuration parameter

## 1.1.1 (2021-06-05)

**Changed:**
* Lib dependencies

## 1.1.0 (2021-01-31)

**Changed:**
* Switched to Vert.x 4.0.0

## 1.0.2 (2020-06-18)

**Changed:**
* Serialize pipe startTime as ISO standard string

## 1.0.1 (2020-02-28)

**Changed:**
* Update connector lib

## 1.0.0 (2019-11-08)

**Added:**
* buildInfo.json for build info via `/health` path
* config.schema.json
* `PIVEAU_LOG_LEVEL` for configuring the general log level of the `io.piveau` package
* Configuration change listener
* `sendHash` pipe configuration parameter
* `sendHash` to config schema

**Changed:**
* `PIVEAU_` prefix to logstash configuration environment variables
* Optional canonical hash attachment on dataInfo
* Requires now latest LTS Java 11
* Docker base image to openjdk:11-jre

**Fixed:**
* Updated all dependencies

## 0.1.0 (2019-05-17)

**Added:**
* `catalogue` read from configuration and pass it to the info object
* Environment `PIVEAU_IMPORTING_SEND_LIST_DELAY` for a configurable delay
* `sendListData` pipe configuration option

**Changed:**
* Readme
* Default output format to `application/n-triples`

**Removed:**
* `mode` configuration and fetchIdentifier

## 0.0.1 (2019-05-03)

Initial release
